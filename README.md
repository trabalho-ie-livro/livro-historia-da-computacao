# Introdução a Engenharia de Computação

Este livro está sendo escrito pelos alunos do Curso de Engenharia da Computação da Universidade Tecnológica do Paraná. O livro é faz parte da disciplina de Introdução a Engenharia e os alunos que participaram da edição estão [listados abaixo](#Autores).

## Índice

1. [*Surgimento das Calculadoras Mecânicas*](capitulos/surgimento_das_calculadoras_mecanicas.md)
1. [*Primeiros Computadores*](capitulos/primeiro-computador.md)
1. [*Evolução dos Computadores Pessoais e sua Interconexão*](capitulos/evolucao-pc.md)
1. [*Computação Móvel*](capitulos/computacao-movel.md)
1. [*Futuro*](capitulos/futuro.md)




## Autores
Esse livro foi escrito por:


| Avatar | Nome | Nickname | Email |
| ------ | ---- | -------- | ----- | 
![](/imagens/gabrielgusn.png)  | Gabriel Gustavo Nicolodi | gabrielgusn | [gnicolodi@alunos.utfpr.edu.br](mailto:gnicolodi@.alunos.utfpr.edu.br)

| Avatar | Nome | Nickname | Email |
| ------ | ---- | -------- | ----- | 
![](https://gitlab.com/uploads/-/system/user/avatar/9168497/avatar.png?width=400)  | Eduardo Jose Gnoatto  | EduardoGnoatto | [Eduardojosegnoatto@alunos.utfpr.edu.br](mailto:eduardojosegnoatto@.alunos.utfpr.edu.br)

| Avatar | Nome | Nickname | Email |
| ------ | ---- | -------- | ----- | 
![](https://pbs.twimg.com/media/E5ziCZAXIAQWiS8?format=png&name=240x240) | Nicolas Costa Stefani | nicolasstefani | [nicolasstefani@alunos.utfpr.edu.br](mailto:nicolasstefani@alunos.utfpr.edu.br)

| Avatar | Nome | Nickname | Email |
| ------ | ---- | -------- | ----- | 
![](https://gitlab.com/uploads/-/system/user/avatar/9175389/avatar.png?width=400)  | Guilherme Santos Reis  | guisreis03 | [guilhermereis.2003@alunos.utfpr.edu.br](mailto:guilhermereis.2003@.alunos.utfpr.edu.br)
