# Computação Móvel 

A utilização de computação e comunicação móvel contribui para aumentar e qualificar a eficiência dos processos envolvidos na aquisição, validação e análise dos processos de inspeção de redes, tanto os associados a manutenções preventivas quanto aos associados a manutenções corretivas (Pretto et al., 2003b) (Silva et al., 2003).. Esse tipo de processo diminui a ocorrência de erros devido às falhas humanas no preenchimento de questionários, identificação de causas de eventos, erros em inspeções de campo, em rotinas de manutenção e aumenta a disponibilidade de informações importantes para o funcionamento do sistema através da aquisição remota. 

Tecnologias de computação e comunicação móvel estão em contínuo avanço em termos de disponibilidade, funcionalidade e custos, tornando-se atraentes para os planos de automatização das empresas. Essas tecnologias permitem uma grande variedade de aplicações, sendo a escolha de uma ou outra, ponderada pelas características do problema. Dentre essas tecnologias pode-se citar o Personal Digital Assistant - PDA, conhecido como Palm-Top, e a comunicação de dados via rede de telefonia celular. 

O que é interessante ressaltar é que existem outros termos que denotam a computação móvel. Dentre eles temos computação ubíqua, computação pervasiva ou ainda computação nômade, com pequenas diferenças entre estes. 

Esses termos estão relacionados com a computação móvel, que segundo [LITKE 2004] é um termo genérico que incorpora aplicações para dispositivos de pequeno porte, portáveis com comunicação e computação sem fio. Estes dispositivos podem ser notebooks, com tecnologia de comunicação sem fio, smartphones, celulares e PDAs (Personal Digital Assistants). Envolve então vários elementos como hardware, dados, aplicações e usuários que tem a capacidade de se moverem para diferentes localizações. Portanto considera-se computação móvel (mobile), de forma geral, a computação onde todos os elementos do sistema têm a propriedade de mobilidade. 

 Já entrando neste assunto podemos exemplificar alguns exemplos de computação móvel: 

   ##              PDA (Personal Digital Assistant) 

PDAs são dispositivos portáteis com tela monocromática ou colorida e sistema de entrada de dados por tela sensível ao toque (touch screen).  
![](https://analysistecnologia.files.wordpress.com/2012/01/pda-1.jpg)
 

## Smartphones 

Os Smartphones são telefones celulares com função de PDA,  

 ![](https://www.kaspersky.com.br/content/pt-br/images/repository/isc/2020/9910/vpn-for-smartphones-a-guide-1.jpg)

 

 Atualmente a maior desvantagem é o tamanho da tela, a qual, geralmente, é menor que a de um PDA. Os Smartphones com o mesmo tamanho de tela de um PDA são chamados de WDA (Wireless Digital Assistants). A convergência entre os smarthphones e os PDAs é considerada natural (CIGRE, 2002). 

 

## Handheld PC 

O handheld PC é similar ao PDA no sentido que eles são terminais com tela colorida e sistema de entrada de dados por tela sensível ao toque (touch screen), porém com um tamanho de tela bem maior, geralmente com a resolução de 800 x 600 pixels ou mais. Os handhelds utilizam os mesmos sistemas operacionais que um PC de mesa, e costumam ser chamados também de Tablet PC (CIGRE, 2002). 

![](https://www.handheldsystems.com/news/archives/hpc2000/fex.jpg)
 

 

## Terminais Dedicados 

Terminais com interface gráfica geralmente baseados em tela de cristal líquido (LCD - Liquid Crystal Display) monocromático alfanumérico e sem a capacidade de programação pelo usuário. Um exemplo disso são os sistemas via satélite para coleta de dados (CIGRE, 2002). 

## Telefones Celulares 

Uma das tecnologias de computação móvel mais difundida atualmente são os telefones celulares, os quais apresentam um baixo custo de aquisição, ampla cobertura de serviços e uma grande potencialidade de utilização como dispositivo de aquisição de dados. Outra grande vantagem do telefone celular é permitir a sua utilização para comunicação de voz e dados. Geralmente possuem telas coloridas, porém com tamanho reduzido, o que muitas vezes limita o tipo de aplicação que pode ser suportada pelo dispositivo. Alguns possuem funções extras como câmera fotográfica, o que permite o desenvolvimento de aplicativos com capacidade de armazenamento fotográfico. Alguns dispositivos utilizam o sistema operacional Symbian OS®. Outros utilizam uma máquina virtual Java para executar aplicativos desenvolvidos por terceiros (CIGRE, 2002). 

## SISTEMA PARA AQUISIÇÃO DE DADOS DE EVENTOS NÃO PROGRAMADOS 

Atualmente os operadores e eletricistas são os analistas dos defeitos que ocorrem nas redes elétricas. Muitas vezes não há possibilidade de identificação direta da causa que originou a parada no fornecimento de energia. A correta identificação da causa de um evento está diretamente relacionada à quantidade e à qualidade da informação no local do evento, bem como da identificação correta das atividades e dos fatos que poderiam indicar a causa de uma falta de energia não programada. Para auxiliar o eletricista nesta atividade, foi desenvolvido um sistema de coleta de dados com a utilização de tecnologia móvel do tipo PDA. 

![](https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSq1NxlXLABjK4UgH8GUnTBHrs_vH3GpJ_0a_XjLNETQJkvQumMF0JnxJG20zh8Y2eFEg4&usqp=CAU)
 

## Sistema de Coleta 

O objetivo da detecção da causa (Billinton et al., 2002) de paralisação do fornecimento de energia é a de suprir as áreas da empresa que gerencia a rede elétrica, com informações sobre o desempenho dos elementos que compõem as mesmas, identificando como posicionar o investimento necessário para redução de problemas e conseqüente ampliação de benefícios (Billinton et al., 1991). Nesse caso, é necessário identificar os elementos básicos de um sistema de distribuição para melhor medir o desempenho desses elementos. Sendo assim, pode-se citar alguns objetivos básicos de interesse nos elementos, sendo eles: 

Identificar quais áreas possuem piores desempenhos das redes; 

Identificar qualidade dos equipamentos instalados no sistema de distribuição; 

Identificar possibilidade de mau gerenciamento dos ativos por parte dos técnicos que atuam na rede; 

Identificar o desempenho de algum tipo de equipamento que seja necessário em alguma região; 

Identificar problemas de redes elétricas mal construídas. 

## Elementos de Interrupção 

As falhas que ocorrem em um sistema de distribuição acontecem diretamente em algum dos elementos que compõem a estrutura física da rede elétrica. Baseado nisso, criou-se o conceito de "elemento de interrupção", representando todas as estruturas que podem apresentar defeitos e, consequentemente, levar à interrupção do fornecimento de energia elétrica (Rosa et al., 2004). Os elementos de interrupção podem ser identificados como: 

* Postes; 

* Cruzetas; 

* Isoladores; 

* Elementos de Sustentação; 

* Condutores; 

* Equipamentos. 

 



## Implementação do Aplicativo de Coleta 

O questionário foi implementado sob forma de um aplicativo desenvolvido em uma estrutura orientada a objetos, onde os dados recolhidos são armazenados em um arquivo local para posteriormente ser enviado a um banco de dados. 

## Java 

A linguagem Java é uma linguagem multiplataforma onde os aplicativos são executados por uma máquina virtual. O compilador Java gera um bytecode que é interpretado por uma máquina virtual específica para cada sistema operacional. Existe uma máquina virtual Java para a maioria dos computadores portáteis e telefones móveis atuais, e isto possibilitou o desenvolvimento do aplicativo utilizando essa linguagem. Toda a interface homem-máquina (IHM) do aplicativo foi "desenhada" utilizando a classe Canvas com os métodos de desenho apropriados. O usuário podia entrar com os dados com o uso da caneta touch screen ou através das teclas de acesso. 

![](https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTlmAIy1NDbGfIdrBvwOLqvER_WuKxIKjZIdg&usqp=CAU)
 

## Embedded Visual Basic 

A empresa Microsoft disponibiliza gratuitamente a ferramenta Embedded Visual Basic para desenvolvimento a aplicativos voltados aos sistemas operacionais Pocket PC e Windows CE. Essa ferramenta permite o desenvolvimento rápido de aplicativos, com várias ferramentas para construção de IHM.  

 ![](https://www.researchgate.net/profile/Juan-Carlos-Olivares-Rojas/publication/274567771/figure/fig2/AS:392025288527887@1470477520804/Figura-2-Mini-aplicacion-en-eMbedded-Visual-Basic-IIIII-HERRAMIENTAS-DE-CODIGO.png)

## C++ 

Utilizou-se a linguagem C++ (Meiler et al.,2001) com o uso da ferramenta de desenvolvimento CodeWarriror® para desenvolver uma versão definitiva do aplicativo de coleta, com todas as funcionalidades necessárias para o sistema operacional Palm OS®. O uso dessa ferramenta se mostrou o mais vantajoso das utilizadas, pois apresentou uma boa velocidade de desenvolvimento com recursos ilimitados de programação. O desempenho obtido foi muito satisfatório, proporcionando uma interação rápida e eficiente do aplicativo com o usuário.  

 ![](https://codelogica.files.wordpress.com/2018/02/c31.jpg?w=1245)

 
 


[Anterior](../README.md)